/**
 * The operator and their corresponding evaluator.
 */
module.exports = {
  '===': (val1, val2) => val1 === val2,
  '!==': (val1, val2) => val1 !== val2,
  '==': (val1, val2) => val1 == val2,
  '!=': (val1, val2) => val1 != val2,
  '>': (val1, val2) => val1 > val2,
  '>=': (val1, val2) => val1 >= val2,
  '<': (val1, val2) => val1 < val2,
  '<=': (val1, val2) => val1 <= val2
};
