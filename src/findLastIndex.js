/**
 * Get the last index that passes the callback.
 *
 * @param {any[]} array the array to traverse
 * @param {(value: any, index: Number) => Boolean} callback the callback to use
 * @returns {Number} the index of the found entry, if not found, returns -1
 */
const findLastIndex = (array, callback) => {
  const length = array.length;
  for (let i = array.length - 1; i >= 0; --i) {
    if (callback(array[i], i)) return i;
  }

  return -1;
};

module.exports = findLastIndex;
