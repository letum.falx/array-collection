/**
 * Wrap the function so that it will only called with the first
 * two parameters only.
 * @param {Function} fn the function to wrap
 * @returns {(value: any, index: Number) => any}
 */
module.exports = fn => (value, index) => fn(value, index);
