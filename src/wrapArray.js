const Collection = require('./Collection');

/**
 * Wraps the given data to array if it is not yet an array.
 *
 * @param {any|any[]} data the data to wrap
 * @returns {any[]} the wrapped data
 */
const wrapArray = data => Array.isArray(data) ? data : [data];

module.exports = wrapArray;
