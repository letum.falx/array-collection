enum Operator {
  '==',
  '===',
  '!=',
  '!==',
  '>',
  '>=',
  '<',
  '<='
};

class Collection<T> {
  /**
   * Create a collection from the given array.
   * The collection will only wrap the given array,
   * no cloning occured. So if the passed array is
   * mutated elsewhere, it will also affect the array
   * on the collection.
   *
   * @param data the array to wrap, default to an empty array if not given
   */
  constructor (data: T);

  /**
   * Create a shallow copy of the internal array,
   * then create a collection from it.
   *
   * @returns the newly created collection from the shallow copy of the internal array
   */
  clone (): Collection<T>;

  /**
   * Checks if the collection contains the given value.
   *
   * @param value the value to check
   * @returns true if the value is in the collection, otherwise false
   */
  contains (value: T): Boolean;

  /**
   * Checks if the collection has an entry that passes
   * the given search function.
   *
   * @param search the search function to use, will pass the entry and its index as the first and second parameter respectively, then it should return true if it pass and false if not
   * @returns true if at least one entry passes the search function, otherwise false
   */
  contains (search: (value: T, index: Number) => Boolean): Boolean;

  /**
   * Checks if there is an entry in the collection that
   * has the given value at the given property key.
   *
   * @param key the property to use for searching
   * @param value the value of the property to find
   *
   * @returns true if collection found a value, otherwise false
   */
  contains<V> (key: String, value: V): Boolean;

  /**
   * Checks if there is an entry that has value on the given property key
   * that returns true on the given operation.
   *
   * @param key the property to use for searching
   * @param operator the operator to use
   * @param value the value to compare
   *
   * @returns true if collection found a value, otherwise false
   */
  contains<V> (key: String, operator: Operator, value: V): Boolean;

  /**
   * Get the number of entries on the collection.
   */
  count (): Number;

  /**
   * Calls the iterator to each entry on the collection.
   *
   * @param iterator the function to call on each entry
   * @returns the current collection instance
   */
  each (iterator: (value: T, index: Number) => void): this;

  /**
   * Removes all the entry of this collection.
   */
  empty (): this;

  /**
   * Determines whether all the entry of the collection has a truthy value
   * on the given property key.
   *
   * @param key the key to check
   */
  every (key: String): Boolean;

  /**
   * Determines whether all the entry of the collection passes the given test
   * function.
   *
   * @param testFunc the test function to use
   */
  every (testFunc: (value: T, index: Number) => Boolean): Boolean;

  /**
   * Determines if all entries of the collection has the value on the
   * specified property key.
   *
   * @param key the property key to check
   * @param value the value to compare
   */
  every<V> (key: String, value: V): Boolean;

  /**
   * Determines if all entries of the collection passes the test function
   * on the specified property key.
   *
   * @param key
   * @param testFunction
   */
  every<V> (key: String, testFunction: (value: V, index: Number) => Boolean): Boolean;

  /**
   * Determines if all entries of the collection passes the given operation.
   *
   * @param key the property key to check
   * @param operator the operator to use
   * @param value the value to compare
   */
  every<V> (key: String, operator: Operator, value: V): Boolean;

  /**
   * Filters out falsy entries.
   *
   * @returns
   */
  filter (): Collection<T>;

  /**
   * Filters out entries that fails the given test function.
   *
   * @param testFunction the function to use for filtering out
   * @returns a new collection containing the remaining entries
   */
  filter (testFunction: (value: T, index: Number) => Boolean): Collection<T>;

  /**
   * Filters out entries that has falsy property key.
   *
   * @param key the property key to use
   * @returns a new collection containing the remaining entries
   */
  filter (key: String): Collection<T>;

  /**
   * Filters out entries that do not have the value on the property key.
   *
   * @param key the property key to search
   * @param value the value to compare
   * @returns a new collection containing the remaining entries
   */
  filter<V> (key: String, value: V): Collection<T>;

  /**
   * Filters out entries that fails the test function on the property key.
   *
   * @param key the property key to search
   * @param testFunction the test to use
   * @returns a new collection containing the remaining entries
   */
  filter<V> (key: String, testFunction: (value: V, index: Number) => Boolean): Collection<T>;

  /**
   * Filters out entries that fails the operation.
   *
   * @param key the property key to use
   * @param operator the operator to use
   * @param value the value to compare
   * @returns a new collection containing the remaining entries
   */
  filter<V> (key: String, operator: Operator, value: V): Collection<T>;

  /**
   * Get the first entry of the collection.
   *
   * @returns the first entry of the collection, if empty, returns NaN
   */
  first (): T;

  /**
   * Get the first entry of the collection that passes the search function.
   *
   * @param searchFunc the function used for searching for the first value
   * @returns the first entry of the collection that passes the search function, NaN if none is found
   */
  first (searchFunc: (entry: T) => Boolean): T;

  /**
   * Get the first entry of the collection.
   *
   * @param defaultValue the value to return in case the collection is empty
   * @returns the first entry of the collection, if empty, returns the default value
   */
  first<V> (defaultValue: V): T;

  /**
   * Get the first entry of the collection that passes the search function.
   *
   * @param searchFunc the function used for searching for the first value
   * @returns the first entry of the collection that passes the search function, the default value if none is found
   */
  first<V> (searchFunc: (entry: T) => Boolean, defaultValue: V): T;

  /**
   * Remove the first occurence of the entry from the collection.
   *
   * @param value the entry to remove
   */
  forget (value: T): this;

  /**
   * Remove the first entry that has a property key with value.
   *
   * @param key the property key to search
   * @param value the value to search
   */
  forget<V> (key: String, value: V): this;

  /**
   * Remove the first entry that has property key with value that passes
   * the given test function.
   *
   * @param key the property key to search
   * @param value the value to search
   */
  forget<V> (key: String, testFunction: (value: V, index: Number) => Boolean): this;

  /**
   * Remove the first entry that passes the given operator.
   *
   * @param key the property key used for operation
   * @param operator the operator to use
   * @param value the value to compare
   */
  forget<V> (key: String, operator: Operator, value: V): this;

  /**
   * Remove the entry with the following index. This will not leave
   * the index empty, so the next entries to the removed index
   * will be moved.
   *
   * @param index the index to forget
   */
  forgetIndex (index: Number): this;

  /**
   * Remove the last occurence of the entry from the collection.
   *
   * @param value the entry to remove
   */
  forgetLast (value: T): this;

  /**
   * Remove the last entry that has a property key with value.
   *
   * @param key the property key to search
   * @param value the value to search
   */
  forgetLast<V> (key: String, value: V): this;

  /**
   * Remove the last entry that has property key with value that passes
   * the given test function.
   *
   * @param key the property key to search
   * @param value the value to search
   */
  forgetLast<V> (key: String, testFunction: (value: V, index: Number) => Boolean): this;

  /**
   * Remove the last entry that passes the given operator.
   *
   * @param key the property key used for operation
   * @param operator the operator to use
   * @param value the value to compare
   */
  forgetLast<V> (key: String, operator: Operator, value: V): this;

  /**
   * Get the value on the given index.
   *
   * @param index the index where to get value
   * @param defaultValue the value to return whenever the index does not exists
   * @returns the value found, or the default value if index does not exists
   */
  get<V> (index: Number, defaultValue: V): T;

  /**
   * Get the underlying array this collection is wrapping.
   */
  getOriginal (): T[];

  /**
   * Checks if the index exists on the collection.
   *
   * @param index the index to check
   */
  has (index: Number): Boolean;

  /**
   * Get the array of indices wrapped in a collection.
   *
   * @returns new collection containing the indices
   */
  indices(): Collection<Number>;

  /**
   * Determines if the collection is empty or not.
   */
  isEmpty (): Boolean;

  /**
   * Determines if the collection has entries.
   */
  isNotEmpty (): Boolean;

  /**
   * Get the last entry of the collection.
   *
   * @returns the last entry of the collection, if empty, returns NaN
   */
  last (): T;

  /**
   * Get the last entry of the collection that passes the search function.
   *
   * @param searchFunc the function used for searching for the last value
   * @returns the last entry of the collection that passes the search function, NaN if none is found
   */
  last (searchFunc: (entry: T) => Boolean): T;

  /**
   * Get the last entry of the collection.
   *
   * @param defaultValue the value to return in case the collection is empty
   * @returns the last entry of the collection, if empty, returns the default value
   */
  last<V> (defaultValue: V): T;

  /**
   * Get the last entry of the collection that passes the search function.
   *
   * @param searchFunc the function used for searching for the last value
   * @returns the last entry of the collection that passes the search function, the default value if none is found
   */
  last<V> (searchFunc: (entry: T) => Boolean, defaultValue: V): T;

  /**
   * Get the last index of this collection.
   *
   * @returns the last index, or -1 if empty
   */
  lastIndex (): Number;

  /**
   * Maps the entries of this collection using the given transformer.
   *
   * @param transformer the function used to transforms the entry
   * @returns a new collection containing the transformed values
   */
  map<V> (transformer: (value: T, index: Number) => V): Collection<V>;

  /**
   * Removes the last entry of the collection, then returns it.
   *
   * @param defaultValue the value to return in case the collection is empty
   */
  pop<V> (defaultValue: V): T;

  /**
   * Adds an entry at the start of the collection.
   *
   * @param value the value to prepend
   */
  prepend (value: T): this;

  /**
   * Adds entries at the start of the collection.
   *
   * @param values the array of values to prepend
   */
  prepend (values: T[]): this;

  /**
   * Removes an entry with the given index, then returns it.
   *
   * @param index the index where to pull
   * @param defaultValue the value to return in case the index does not exists
   * @returns the pulled entry, or the default value if index does not exists
   */
  pull<V> (index: Number, defaultValue: V): T;

  /**
   * Adds an entry at the end of the collection.
   *
   * @param value the value to push
   */
  push (value: T): this;

  /**
   * Adds entries at the end of the collection.
   *
   * @param values the array of values to push
   */
  push (values: T[]): this;

  /**
   * Get a random entry from the collection.
   *
   * @param defaultValue the default value in case the collection is empty
   * @returns a random entry from the collection or the default value in case the collection is empty
   */
  random<V> (defaultValue: V): T;

  /**
   * Pull a random entry from the collection.
   *
   * @param defaultValue the default value in case the collection is empty
   * @returns a random entry from the collection or the default value in case the collection is empty
   */
  randomPull<V> (defaultValue: V): T;

  /**
   * Reduce the collection using a reducer function.
   *
   * @param reducer the function to use
   * @param initialValue the initial seed to use
   */
  reduce<I, A, R> (reducer: (accumulator: A, current: T, index: Number) => R, initialValue: I): R;

  /**
   * Filters out truthy entries.
   *
   * @returns
   */
  reject (): Collection<T>;

  /**
   * Filters out entries that passes the given test function.
   *
   * @param testFunction the function to use for filtering out
   * @returns a new collection containing the remaining entries
   */
  reject (testFunction: (value: T, index: Number) => Boolean): Collection<T>;

  /**
   * Filters out entries that has truthy property key.
   *
   * @param key the property key to use
   * @returns a new collection containing the remaining entries
   */
  reject (key: String): Collection<T>;

  /**
   * Filters out entries that do have the value on the property key.
   *
   * @param key the property key to search
   * @param value the value to compare
   * @returns a new collection containing the remaining entries
   */
  reject<V> (key: String, value: V): Collection<T>;

  /**
   * Filters out entries that passes the test function on the property key.
   *
   * @param key the property key to search
   * @param testFunction the test to use
   * @returns a new collection containing the remaining entries
   */
  reject<V> (key: String, testFunction: (value: V, index: Number) => Boolean): Collection<T>;

  /**
   * Filters out entries that passes the operation.
   *
   * @param key the property key to use
   * @param operator the operator to use
   * @param value the value to compare
   * @returns a new collection containing the remaining entries
   */
  reject<V> (key: String, operator: Operator, value: V): Collection<T>;

  /**
   * Create a new collection with entries' sequence reversed.
   *
   * @returns a new collection
   */
  reverse (): Collection<T>;

  /**
   * Get the index of the first entry with the following value.
   *
   * @param value the value to search
   * @returns the index if found, otherwise -1
   */
  search (value: T): Number;

  /**
   * Get the index of the first entry that has a property key with value.
   *
   * @param key the property key to search
   * @param value the value to search
   * @returns the index if found, otherwise -1
   */
  search<V> (key: String, value: V): Number;

  /**
   * Get the index of the first entry that has property key with value that
   * passes the given test function.
   *
   * @param key the property key to search
   * @param value the value to search
   * @returns the index if found, otherwise -1
   */
  search<V> (key: String, testFunction: (value: V, index: Number) => Boolean): Number;

  /**
   * Get the index of the first entry that passes the given operator.
   *
   * @param key the property key used for operation
   * @param operator the operator to use
   * @param value the value to compare
   * @returns the index if found, otherwise -1
   */
  search<V> (key: String, operator: Operator, value: V): Number;

  /**
   * Get the index of the last entry with the following value.
   *
   * @param value the value to search
   * @returns the index if found, otherwise -1
   */
  searchLast (value: T): Number;

  /**
   * Get the index of the last entry that has a property key with value.
   *
   * @param key the property key to search
   * @param value the value to search
   * @returns the index if found, otherwise -1
   */
  searchLast<V> (key: String, value: V): Number;

  /**
   * Get the index of the last entry that has property key with value that
   * passes the given test function.
   *
   * @param key the property key to search
   * @param value the value to search
   * @returns the index if found, otherwise -1
   */
  searchLast<V> (key: String, testFunction: (value: V, index: Number) => Boolean): Number;

  /**
   * Get the index of the last entry that passes the given operator.
   *
   * @param key the property key used for operation
   * @param operator the operator to use
   * @param value the value to compare
   * @returns the index if found, otherwise -1
   */
  searchLast<V> (key: String, operator: Operator, value: V): Number;

  /**
   * Pulls out the first entry of the collection.
   *
   * @param defaultValue the value to return in case the collection is empty
   * @returns the pulled value if there is one, otherwise the default value
   */
  shift<V> (defaultValue: V): T;

  /**
   * Slice the collection. Follows the same rule with array's slice.
   *
   * @param startIndex the starting index of slice
   * @param endIndex the end index of slice
   * @returns a new collection containing the sliced part
   */
  slice (startIndex: Number, endIndex: Number): Collection<T>;

  /**
   * Sort the collection in ascending order.
   *
   * @returns a new collection containing the sorted entries
   */
  sort (): Collection<T>;

  /**
   * Sort the collection in ascending order using a sorting function.
   *
   * @param sortFunc the sorting function to use
   * @returns a new collection containing the sorted entries
   */
  sort (sortFunc: (a: T, b: T) => Number): Collection<T>;

  /**
   * Sort the collection in ascending order using the value of
   * the property key.
   *
   * @param key the property key of the value used for sorting
   * @returns a new collection containing the sorted entries
   */
  sort (key: String): Collection<T>;

  /**
   * Sort the collection in ascending order using the value of
   * the property key and a sorting function.
   *
   * @param key the property key of the value used for sorting
   * @param sortFunc the function used for sorting
   * @returns a new collection containing the sorted entries
   */
  sort<V> (key: String, sortFunc: (a: V, b: V) => Number): Collection<T>;

  /**
   * Sort the collection in descending order.
   *
   * @returns a new collection containing the sorted entries
   */
  sortDesc (): Collection<T>;

  /**
   * Sort the collection in descending order using a sorting function.
   *
   * @param sortFunc the sorting function to use
   * @returns a new collection containing the sorted entries
   */
  sortDesc (sortFunc: (a: T, b: T) => Number): Collection<T>;

  /**
   * Sort the collection in descending order using the value of
   * the property key.
   *
   * @param key the property key of the value used for sorting
   * @returns a new collection containing the sorted entries
   */
  sortDesc (key: String): Collection<T>;

  /**
   * Sort the collection in descending order using the value of
   * the property key and a sorting function.
   *
   * @param key the property key of the value used for sorting
   * @param sortFunc the function used for sorting
   * @returns a new collection containing the sorted entries
   */
  sortDesc<V> (key: String, sortFunc: (a: V, b: V) => Number): Collection<T>;

  /**
   * Splice the collection, removing the part being sliced.
   * Follows the same rule with array's splice.
   *
   * @param startIndex the starting index of slice
   * @param endIndex the end index of slice
   * @param value the value to insert
   * @returns a new collection containing the sliced part
   */
  splice (startIndex: Number, endIndex: Number, value: T): Collection<T>;

  /**
   * Splice the collection, removing the part being sliced.
   * Follows the same rule with array's splice, but the third
   * argument accepts an array of values to insert.
   *
   * @param startIndex the starting index of slice
   * @param endIndex the end index of slice
   * @param value the array of values to insert
   * @returns a new collection containing the sliced part
   */
  splice (startIndex: Number, endIndex: Number, value: T[]): Collection<T>;

  /**
   * Splice the collection, removing the part being sliced.
   * Follows the same rule with array's splice, but the third
   * argument accepts collection of values to insert.
   *
   * @param startIndex the starting index of slice
   * @param endIndex the end index of slice
   * @param value the collection of values to insert
   * @returns a new collection containing the sliced part
   */
  splice (startIndex: Number, endIndex: Number, value: Collection<T>): Collection<T>;

  /**
   * Generate a JSON string from this collection.
   */
  toJson (): String;

  /**
   * Calls the if handler with this collection as first argument if condition finalized as truthy,
   * otherwise call the else handler with this collection as first argument.
   *
   * @param condition the condition to determine whether to execute the if or else handler
   * @param ifHandler the handler if the condition is truthy
   * @param elseHandler the handler if the condition is falsy
   * @returns this collection instance
   */
  when (condition: Boolean, ifHandler: (collection: this) => void, elseHandler: (collection: this) => void): this;
};

export = Collection;
