/**
 * Get the array of indices for the given array.
 *
 * @param {any[]} array the array to traverse
 * @returns {Number[]} the indices of the array
 */
const indices = array => {
  return array.map((_, index) => index);
};

module.exports = indices;