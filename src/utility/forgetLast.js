const searchLast = require('./searchLast');
const splice = require('./splice');

/**
 * Forgets the last value to pass the condition.
 *
 * @param {any[]} array the array where we want to forget
 * @param {IArguments} args the arguments passed
 * @returns {Boolean} true if successfully forgotten, otherwise false
 */
const forgetLast = (array, args) => {
  const index = searchLast(array, args);
  if (index > -1) {
    splice(array, [index, 1]);
    return true;
  }

  return false;
};

module.exports = forgetLast;
