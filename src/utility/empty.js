/**
 * Removes all the entry of the array.
 *
 * @param {any[]} array the array to empty
 * @returns {Array} an empty array
 */
const empty = array => {
  array.splice(0);
  return array;
};

module.exports = empty;
