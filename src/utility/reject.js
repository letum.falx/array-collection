const getProp = require('../getProp');
const operators = require('../operators');

/**
 * Filters out entries of the array that passes the condition.
 *
 * @param {any[]} array the array to traverse
 * @param {IArguments} args the arguments passed
 *
 * @returns {any[]} the new filtered array
 */
const reject = (array, args) => {
  switch (args.length) {
    case 0:
      return array.filter(value => !value);
    case 1: {
      const rejectFunc = typeof args[0] === 'function'
        ? (value, index) => !args[0](value, index)
        : value => !getProp(value, args[0]);
      return array.filter(rejectFunc);
    }
    case 2: {
      const rejectFunc = typeof args[1] === 'function'
        ? (val, index) => !args[1](getProp(val, args[0]), index)
        : val => getProp(val, args[0]) !== args[1];
      return array.filter(rejectFunc);
    }
    case 3:
      if (!operators[args[1]]) {
        throw new TypeError('invalid operator');
      }
      return reject(array, [args[0], value => operators[args[1]](value, args[2])]);
    default:
      throw new Error('can only accept up to three arguments');
  }
};

module.exports = reject;
