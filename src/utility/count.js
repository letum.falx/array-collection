/**
 * Count the entry on an array.
 *
 * @param {any[]} array the array to count
 * @returns {Number} the total entry on the array
 */
const count = array => {
  return array.length;
};

module.exports = count;
