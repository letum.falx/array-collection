/**
 * Reverse the current sequence of the array.
 *
 * @param {any[]} array the array to reverse
 * @returns {any[]} the new array containing the reversed sequence
 */
const reverse = array => array.reduceRight((arr, curr) => {
  arr.push(curr);
  return arr;
}, []);

module.exports = reverse;
