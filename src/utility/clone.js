const slice = require('./slice');

/**
 * Shallow clone the given array.
 *
 * @param {any[]} array the array to clone
 * @returns a new array
 */
const clone = array => array.slice(0);

module.exports = clone;
