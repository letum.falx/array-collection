const search = require('./search');

/**
 * Checks whether given value is contained in the array.
 *
 * @param {any[]} array the array to traverse
 * @param {IArguments} args the arguments passed
 *
 * @returns {Boolean} true if the value is inside the array, otherwise false
 */
const contains = (array, args) => search(array, args) > -1;

module.exports = contains;
