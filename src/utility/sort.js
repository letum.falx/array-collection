const getProp = require('../getProp');

/**
 * Sort the array.
 *
 * @param {any[]} array the array to sort
 * @param {IArguments} args the arguments passed
 * @returns {any[]} new sorted array
 */
const sort = (array, args) => {
  array = array.slice(0);
  switch (args.length) {
    case 0:
      return array.sort();
    case 1: {
      const sortFunc = typeof args[0] === 'function'
        ? (a, b) => args[0](a, b)
        : (a, b) => getProp(a, args[0]) - getProp(b, args[0]);

      return array.sort(sortFunc);
    }
    case 2: {
      if (typeof args[1] !== 'function') {
        throw new TypeError('second argument should be a function');
      } else if (typeof args[0] === 'function') {
        throw new TypeError('first argument should not be a function if second is a function');
      }

      return array.sort((a, b) => args[1](getProp(a, args[0]), getProp(b, args[0])));
    }
    default:
      throw new Error('only accepting up to two arguments only');
  }
};

module.exports = sort;
