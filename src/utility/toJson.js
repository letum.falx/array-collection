/**
 * Convert the array to a JSON string.
 *
 * @param {any[]} array the array to convert to be a JSON string
 * @returns {String} the JSON string generated
 */
const toJson = array => JSON.stringify(array);

module.exports = toJson;
