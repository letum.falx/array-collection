const wrapFunc = require('../wrapFunc');

/**
 * Calls the iterator on each entry of the array
 *
 * @param {any[]} array the array to traverse
 * @param {IArguments} args the arguments passed
 */
const each = (array, args) => {
  return array.forEach(wrapFunc(args[0]));
};

module.exports = each;
