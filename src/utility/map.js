const wrapFunc = require('../wrapFunc');

/**
 * Maps the array using the transformer given.
 *
 * @param {any[]} array the array to map
 * @param {IArguments} args the arguments passed
 * @return {any[]} the array of transformed value
 */
const map = (array, args) => {
  return array.map(wrapFunc(args[0]));
};

module.exports = map;
