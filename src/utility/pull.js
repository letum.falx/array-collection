const defaultValue = require('../defaultValue');
const splice = require('./splice');
const has = require('./has');

/**
 * Removes an entry from the collection on the given
 * index, and returns it.
 *
 * @param {any[]} array the array to pull out from
 * @param {IArguments} args the arguments passed
 * @return {any} the pulled entry
 */
const pull = (array, args) => has(array, [args[0]]) ? splice(array, [args[0], 1])[0] : defaultValue(args, 1);

module.exports = pull;
