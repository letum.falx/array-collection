const has = require('./has');
const splice = require('./splice');

/**
 * Remove the entry with the given index from the array.
 *
 * @param {any[]} array the array to traverse
 * @param {IArguments} args the arguments passed
 * @return {Boolean} true if successfully forgotten, otherwise false
 */
const forgetIndex = (array, args) => {
  if (has(array, [args[0]])) {
    splice(array, [args[0], 1]);
    return true;
  }

  return false;
};

module.exports = forgetIndex;
