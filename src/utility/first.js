const getProp = require('../getProp');
const search = require('./search');

/**
 * Get the first entry that matches the condition.
 *
 * @param {any[]} array the array to traverse
 * @param {IArguments} args the arguments passed
 * @return {any}
 */
const first = (array, args) => {
  switch (args.length) {
    case 0:
      return getProp(array, '0');
    case 1: {
      if (typeof args[0] === 'function') {
        const index = search(array, [args[0]]);
        return getProp(array, index.toString());
      }

      return getProp(array, '0', args[0]);
    }
    case 2: {
      const index = search(array, [args[0]]);
      return getProp(array, index.toString(), args[1]);
    }
    default:
      throw new Error('can only accept up to two arguments');
  }
};

module.exports = first;
