/**
 * Slice the array.
 *
 * @param {any[]} array the array to slice
 * @param {IArguments} args the arguments passed
 * @returns {any[]} the part of the array that is sliced
 */
const slice = (array, args) => array.slice(args[0], args[1]);

module.exports = slice;
