const count = require('./count');

/**
 * Get the last index of this array.
 *
 * @param {any[]} array the array to check
 * @returns {Number} the last index of the array, -1 if empty
 */
const lastIndex = array => count(array) - 1;

module.exports = lastIndex;