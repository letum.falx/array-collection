const count = require('./count');
const defaultValue = require('../defaultValue');
const isEmpty = require('./isEmpty');

/**
 * Get a random entry from the array.
 *
 * @param {any[]} array the array where we will get the random entry
 * @param {IArguments} args the arguments passed
 *
 * @returns {any} the random item or the default value if no array is empty
 */
const random = (array, args) => {
  if (isEmpty(array)) {
    return defaultValue(args, 0);
  }

  return array[Math.floor(Math.random() * count(array))];
};

module.exports = random;
