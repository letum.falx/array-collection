const wrapArray = require('../wrapArray');
const splice = require('./splice');

/**
 * Adds the given values to the array.
 *
 * @param {any[]} array the array where to add
 * @param {IArguments} args the arguments passed
 * @returns {any[]} the array where we add
 */
const prepend = (array, args) => {
  if (args.length !== 1) {
    throw new Error('accepts one argument only');
  }

  const prependArray = wrapArray(args[0]);

  splice(array, [0, 0, prependArray]);

  return array;
};

module.exports = prepend;
