/**
 * Reduce the array using a function.
 *
 * @param {any[]} array the array to reduce
 * @param {IArguments} args the arguments passed
 */
const reduce = (array, args) => array.reduce((accumulator, current, index) => args[0](accumulator, current, index), args[1]);

module.exports = reduce;
