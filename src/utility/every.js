const wrapFunc = require('../wrapFunc');
const getProp = require('../getProp');
const operators = require('../operators');

/**
 * Checks if the conditions passes to all entries of the array.
 *
 * @param {any[]} array the array to traverse
 * @param {IArguments} args the arguments passed
 * @returns {Boolean} true if all passes, otherwise false
 */
const every = (array, args) => {
  switch (args.length) {
    case 1: {
      const testFunction = typeof args[0] === 'function'
        ? wrapFunc(args[0])
        : value => getProp(value, args[0]);
      return array.every(testFunction);
    }
    case 2: {
      const testFunction = typeof args[1] === 'function'
        ? (value, index) => args[1](getProp(value, args[0]), index)
        : value => getProp(value, args[0]) === args[1];
      return array.every(testFunction);
    }
    case 3:
      if (!operators[args[1]]) {
        throw new TypeError('invalid operator');
      }
      return every(array, [args[0], value => operators[args[1]](value, args[2])]);
    default:
      throw new Error('can only accept one to three arguments');
  }
};

module.exports = every;
