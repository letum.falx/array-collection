const count = require('./count');

/**
 * Determines whether the array is empty or not.
 *
 * @param {any[]} array the array to check
 * @returns {Boolean} true if the array is empty, otherwise false
 */
const isEmpty = array => {
  return count(array) <= 0;
};

module.exports = isEmpty;
