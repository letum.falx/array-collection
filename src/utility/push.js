const wrapArray = require('../wrapArray');

/**
 * Adds entries at the end of the array.
 *
 * @param {any[]} array the array where to push to
 * @param {IArguments} args the list of arguments passed
 * @returns {any[]} the array where we push
 */
const push = (array, args) => {
  if (args.length !== 1) {
    throw new Error('accepts one argument only');
  }

  const pushArray = wrapArray(args[0]);
  Array.prototype.push.apply(array, pushArray);
  return array;
};

module.exports = push;
