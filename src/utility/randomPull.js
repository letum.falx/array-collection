const pull = require('./pull');
const isEmpty = require('./isEmpty');
const count = require('./count');
const defaultValue = require('../defaultValue');

/**
 * Randomly pull an entry from the array.
 *
 * @param {any[]} array the array where we will pull from
 * @param {IArguments} args the arguments passed
 * @returns {any} the pulled value, or the default value if array is empty
 */
const randomPull = (array, args) => {
  if (isEmpty(array)) {
    return defaultValue(args, 0);
  }

  return pull(array, [Math.floor(Math.random() * count(array))]);
};

module.exports = randomPull;
