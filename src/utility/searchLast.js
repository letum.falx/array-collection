const getProp = require('../getProp');
const operators = require('../operators');
const findLastIndex = require('../findLastIndex');

/**
 * Get the index of the first entry that matches the condition.
 *
 * @param {any[]} array the array to traverse
 * @param {IArguments} args the arguments passed
 *
 * @returns {Number} the index of the first entry matches the condition, -1 if not found
 */
const searchLast = (array, args) => {
  switch (args.length) {
    case 1: {
      const searchFunc = typeof args[0] === 'function'
        ? args[0]
        : value => value === args[0];

      return findLastIndex(array, searchFunc);
    }
    case 2:
      const searchFunc = typeof args[1] === 'function'
        ? (value, index) => args[1](getProp(value, args[0]), index)
        : value => getProp(value, args[0]) === args[1];
      return findLastIndex(array, searchFunc);
    case 3:
      if (!operators[args[1]]) {
        throw new TypeError('invalid operator');
      }
      return searchLast(array, [args[0], value => operators[args[1]](value, args[2])]);
    default:
      throw new Error('only accepting one to three arguments only');
  }
};

module.exports = searchLast;
