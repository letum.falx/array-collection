const defaultValue = require('../defaultValue');
const has = require('./has');

/**
 * Get the value on the given index.
 *
 * @param {any[]} array the array to traverse
 * @param {IArguments} args the arguments passed
 *
 * @returns {any} the value found, or the default value if not found
 */
const get = (array, args) => {
  return has(array, args)
    ? array[args[0]]
    : defaultValue(args, 1);
};

module.exports = get;
