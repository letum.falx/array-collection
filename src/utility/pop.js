const isEmpty = require('./isEmpty');
const defaultValue = require('../defaultValue');

/**
 *
 * @param {any[]} array the array to pop
 * @param {IArguments} args the arguments passed
 */
const pop = (array, args) => isEmpty(array) ? defaultValue(args, 0) : array.pop();

module.exports = pop;
