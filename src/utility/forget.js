const search = require('./search');
const splice = require('./splice');

/**
 * Forgets the first value to pass the condition.
 *
 * @param {any[]} array the array where we want to forget
 * @param {IArguments} args the arguments passed
 * @returns {Boolean} true if successfully forgotten, otherwise false
 */
const forget = (array, args) => {
  const index = search(array, args);
  if (index > -1) {
    splice(array, [index, 1]);
    return true;
  }

  return false;
};

module.exports = forget;
