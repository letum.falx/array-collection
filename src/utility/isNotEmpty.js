const count = require('./count');

/**
 * Determines whether the array has entry.
 *
 * @param {any[]} array the array to check
 * @returns {Boolean} true if the array is not empty, otherwise false
 */
const isNotEmpty = array => {
  return count(array) > 0;
};

module.exports = isNotEmpty;
