const wrapArray = require('../wrapArray');

/**
 * Splice the array.
 *
 * @param {any[]} array the array to slice
 * @param {IArguments} args the arguments passed
 * @returns {any[]} the part of the array that is sliced
 */
const splice = (array, args) => {
  const passArgs = [args[0], args[1]];
  if (Object.prototype.hasOwnProperty.call(args, 2)) {
    Array.prototype.push.apply(passArgs, wrapArray(args[2]));
  }

  return Array.prototype.splice.apply(array, passArgs);
};

module.exports = splice;
