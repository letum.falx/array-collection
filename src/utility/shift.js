const defaultValue = require('../defaultValue');
const isEmpty = require('./isEmpty');

/**
 * Pull-out the first entry of the array.
 *
 * @param {any[]} array the array to shift
 * @param {IArguments} args the arguments passed
 * @returns {any} the pull entry, or the default value if the array is empty
 */
const shift = (array, args) => {
  if (isEmpty(array)) {
    return defaultValue(args, 0);
  }

  return array.shift();
};

module.exports = shift;
