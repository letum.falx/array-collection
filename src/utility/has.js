/**
 * Checks whether the array has the index.
 *
 * @param {any[]} array the array to traverse
 * @param {IArguments} args the arguments passed
 *
 * @returns {Boolean} true if array has index, otherwise false
 */
const has = (array, args) => {
  return Object.prototype.hasOwnProperty.call(array, args[0]);
};

module.exports = has;
