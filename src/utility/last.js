const getProp = require('../getProp');
const searchLast = require('./searchLast');

/**
 * Get the last entry that matches the condition.
 *
 * @param {any[]} array the array to traverse
 * @param {IArguments} args the arguments passed
 * @return {any}
 */
const last = (array, args) => {
  const lastIndex = (array.length - 1).toString();
  switch (args.length) {
    case 0:
      return getProp(array, lastIndex);
    case 1: {
      if (typeof args[0] === 'function') {
        const index = searchLast(array, [args[0]]);
        return getProp(array, index.toString());
      }

      return getProp(array, lastIndex, args[0]);
    }
    case 2: {
      const index = searchLast(array, [args[0]]);
      return getProp(array, index.toString(), args[1]);
    }
    default:
      throw new Error('can only accept up to two arguments');
  }
};

module.exports = last;
