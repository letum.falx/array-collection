/**
 * Get the supposed to be default value from the argument given.
 * If not exists, will return NaN.
 *
 * @param {IArguments} args the arguments passed
 * @param {Number} index the index that is supposed to be the default value
 * @returns {any|Number} the value on that index if it exists, otherwise NaN
 */
const defaultValue = (args, index) => Object.prototype.hasOwnProperty.call(args, index) ? args[index] : NaN;

module.exports = defaultValue;
