const clone = require('./utility/clone');
const contains = require('./utility/contains');
const count = require('./utility/count');
const each = require('./utility/each');
const empty = require('./utility/empty');
const every = require('./utility/every');
const filter = require('./utility/filter');
const first = require('./utility/first');
const forget = require('./utility/forget');
const forgetIndex = require('./utility/forgetIndex');
const forgetLast = require('./utility/forgetLast');
const get = require('./utility/get');
const has = require('./utility/has');
const isEmpty = require('./utility/isEmpty');
const isNotEmpty = require('./utility/isNotEmpty');
const indices = require('./utility/indices');
const last = require('./utility/last');
const lastIndex = require('./utility/lastIndex');
const map = require('./utility/map');
const pop = require('./utility/pop');
const prepend = require('./utility/prepend');
const pull = require('./utility/pull');
const push = require('./utility/push');
const random = require('./utility/random');
const randomPull = require('./utility/randomPull');
const reduce = require('./utility/reduce');
const reject = require('./utility/reject');
const reverse = require('./utility/reverse');
const search = require('./utility/search');
const searchLast = require('./utility/searchLast');
const shift = require('./utility/shift');
const slice = require('./utility/slice');
const sort = require('./utility/sort');
const sortDesc = require('./utility/sortDesc');
const splice = require('./utility/splice');
const toJson = require('./utility/toJson');

class Collection {
  constructor (data = []) {
    this._data = data;
  }

  clone () {
    const array = clone(this._data);
    return new Collection(array);
  }

  contains () {
    return contains(this._data, arguments);
  }

  count () {
    return count(this._data, arguments);
  }

  each () {
    return each(this._data, arguments);
  }

  empty () {
    empty(this._data, arguments);
    return this;
  }

  every () {
    return every(this._data, arguments);
  }

  filter () {
    const array = filter(this._data, arguments);
    return new Collection(array);
  }

  first () {
    return first(this._data, arguments);
  }

  forget () {
    forget(this._data, arguments);
    return this;
  }

  forgetIndex () {
    forgetIndex(this._data, arguments);
    return this;
  }

  forgetLast () {
    forgetLast(this._data, arguments);
    return this;
  }

  get () {
    return get(this._data, arguments);
  }

  getOriginal () {
    return this._data;
  }

  has () {
    return has(this._data, arguments);
  }

  indices () {
    return new Collection(indices(this._data));
  }

  isEmpty () {
    return isEmpty(this._data);
  }

  isNotEmpty () {
    return isNotEmpty(this._data);
  }

  last () {
    return last(this._data, arguments);
  }

  lastIndex () {
    return lastIndex(this._data);
  }

  map () {
    const array = map(this._data, arguments);
    return new Collection(array);
  }

  pop () {
    return pop(this._data);
  }

  prepend () {
    prepend(this._data, arguments);
    return this;
  }

  pull () {
    return pull(this._data, arguments);
  }

  push () {
    push(this._data, arguments);
    return this;
  }

  random () {
    return random(this._data, arguments);
  }

  randomPull () {
    return randomPull(this._data, arguments);
  }

  reduce () {
    return reduce(this._data, arguments);
  }

  reject () {
    const array = reject(this._data, arguments);
    return new Collection(array);
  }

  reverse () {
    const array = reverse(this._data, arguments);
    return new Collection(array);
  }

  search () {
    return search(this._data, arguments);
  }

  searchLast () {
    return searchLast(this._data, arguments);
  }

  shift () {
    return shift(this._data, arguments);
  }

  slice () {
    const array = slice(this._data, arguments);
    return new Collection(array);
  }

  sort () {
    const array = sort(this._data, arguments);
    return new Collection(array);
  }

  sortDesc () {
    const array = sortDesc(this._data, arguments);
    return new Collection(array);
  }

  splice () {
    const array = splice(this._data, arguments);
    return new Collection(array);
  }

  toJson () {
    return toJson(this._data);
  }

  when () {
    const args = arguments;
    if (args[0]) {
      typeof args[1] === 'function' && args[1](this);
    } else {
      typeof args[2] === 'function' && args[2](this);
    }

    return this;
  }
}

module.exports = Collection;
