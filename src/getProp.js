const getProp = require('@letumfalx/get-set-prop').getProp;
const blankObject = Object.create(null);

/**
 * Get the property on a deep nested object using a dot notation.
 *
 * @param {any} obj the object to traverse
 * @param {String} key the dot notation of the key to get
 * @param {any} defaultValue the default value to return in case the key is not found
 * @returns {any} the value found, or the default value if not found
 */
module.exports = (obj, key, defaultValue = NaN) => getProp(
  !obj || typeof obj !== 'object'
    ? blankObject
    : obj,
  key,
  defaultValue
);