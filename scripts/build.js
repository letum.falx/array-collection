const path = require('path');
const fs = require('fs').promises;
const babel = require('@babel/core');
const rimraf = require('rimraf');

const sourcePath = path.resolve(__dirname, '..', 'src');
const outputPath = path.resolve(__dirname, '..', 'lib');

const createFileList = async () => {
  const fileList = [];

  const registerFile = async fullPathName => {
    const fileName = path.basename(fullPathName);
    if (fileName.indexOf('.') === 0) return;
    const stat = await fs.stat(fullPathName);
    if (stat.isDirectory()) {
      await fs.readdir(fullPathName)
        .then(async list => {
          for (let i = 0; i < list.length; ++i) {
            await registerFile(path.resolve(fullPathName, list[i]));
          }
        });
    } else {
      fileList.push(fullPathName);
    }
  };

  await registerFile(sourcePath);
  return fileList;
};

/**
 * Babelify and save the result to the output directory.
 *
 * @param {String} fullPathName the full path name of the file to babelify
 */
const babelify = async fullPathName => {
  const outputFile = path.resolve(outputPath, path.relative(sourcePath, fullPathName));
  return await fs.mkdir(path.dirname(outputFile), { recursive: true })
    .then(async () => await babel.transformFileAsync(fullPathName))
    .then(async ({ code }) => await fs.writeFile(outputFile, code));
};

/**
 * Copy the file to the output directory.
 *
 * @param {String} fullPathName the full path name of the file to copy
 */
const copyFile = async fullPathName => {
  const outputFile = path.resolve(outputPath, path.relative(sourcePath, fullPathName));
  return await fs.mkdir(path.dirname(outputFile), { recursive: true })
    .then(async () => await fs.copyFile(fullPathName, outputFile));
};

/**
 * @param {String} fileName
 */
const processFile = async fileName => await (/\.js$/.test(fileName) ? babelify : copyFile)(fileName);

new Promise((resolve, reject) => rimraf(outputPath, error => error ? reject(error) : resolve()))
  .then(() => fs.mkdir(outputPath, { recursive: true }))
  .then(createFileList)
  .then(async list => {
    for (let i = 0; i < list.length; ++i) {
      await processFile(list[i]);
    }
  });
