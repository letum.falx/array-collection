const [collection] = require('./_data');

console.log(collection.isEmpty()); // returns false
console.log(collection.empty().isEmpty()); // returns true
