const [collection] = require('./_data');

console.log(collection.count()); // returns 5

console.log(collection.randomPull()); // displays an entry

console.log(collection.count()); // returns 4

console.log(collection.empty().randomPull(null)); // displays null
