const [collection] = require('./_data');

// original sequence, [ 'John', 'Mary', 'Michael', 'Alice', 'Jose' ]
console.log(collection.map(entry => entry.name).getOriginal());

// reversed sequence, [ 'Jose', 'Alice', 'Michael', 'Mary', 'John' ]
console.log(collection.reverse().map(entry => entry.name).getOriginal());
