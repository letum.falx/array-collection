const [collection] = require('./_data');

const clone = collection.clone();

console.log(clone === collection); // returns false
console.log(clone.getOriginal() === collection.getOriginal()); // returns false
console.log(clone.get(0) === collection.get(0)); // returns true
