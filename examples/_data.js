const Collection = require('../src/Collection');

/**
 * @typedef Birth
 * @property {String} month
 * @property {Number} day
 * @property {Number} year
 */
/**
 * @typedef Data
 * @property {String} name
 * @property {Birth} birth
 * @property {Boolean} registered
 * @property {'A'|'B'|'AB'|'O'} bloodType
 */

/**
 * The initial seed for the collection.
 * @type {Data[]}
 */
const seed = [
  {
    name: 'John',
    birth: {
      month: 'January',
      day: 27,
      year: 2000
    },
    registered: true,
    bloodType: 'O'
  },
  {
    name: 'Mary',
    birth: {
      month: 'January',
      day: 15,
      year: 1995
    },
    registered: false,
    bloodType: 'O'
  },
  {
    name: 'Michael',
    birth: {
      month: 'December',
      day: 18,
      year: 1998
    },
    registered: true,
    bloodType: 'O'
  },
  {
    name: 'Alice',
    birth: {
      month: 'May',
      day: 24,
      year: 1993
    },
    registered: false,
    bloodType: 'O'
  },
  {
    name: 'Jose',
    birth: {
      month: 'September',
      day: 11,
      year: 2002
    },
    registered: true,
    bloodType: 'O'
  }
];

/**
 * @type {[Collection<Data>, Data[]]}
 */
module.exports = [new Collection(seed), seed];
