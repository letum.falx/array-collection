const [collection, seed] = require('./_data');

seed.push(null);

console.log(collection.count()); // returns 6

// passing no arguments
console.log(collection.reject().count()); // returns 1, null is the only one remains

// passing a test function
console.log(collection.reject(value => value && value.name === 'Michael').count()); // returns 5

// passing a property key
console.log(collection.reject('registered').count()); // returns 3

// passing a key and value
console.log(collection.reject('birth.month', 'January').count()); // returns 4

// passing a key and test function
console.log(collection.reject('birth.month', value => value === 'January').count()); // returns 4

// passing a key, operator, and a vallue
console.log(collection.reject('birth.month', '===', 'January').count()); // returns 4
