const [collection, seed] = require('./_data');

// passing a entry value
console.log(collection.clone().forget(seed[0]).map(entry => entry.name).getOriginal()); // returns [ 'Mary', 'Michael', 'Alice', 'Jose' ], 'John' has been forgotten

// passing a search function
console.log(collection.clone().forget(entry => entry.birth.year < 2000).map(entry => entry.name).getOriginal()); // [ 'John', 'Michael', 'Alice', 'Jose' ], 'Mary' has been forgotten

// passing a property key and value
console.log(collection.clone().forget('birth.month', 'January').map(entry => entry.name).getOriginal()); // returns [ 'Mary', 'Michael', 'Alice', 'Jose' ], 'John' has been forgotten

// passing a property key and a search function
console.log(collection.clone().forget('birth.year', year => year < 2000).map(entry => entry.name).getOriginal()); // [ 'John', 'Michael', 'Alice', 'Jose' ], 'Mary' has been forgotten

// passing a property key, operator and value
console.log(collection.clone().forget('birth.year', '<', 2000).map(entry => entry.name).getOriginal()); // [ 'John', 'Michael', 'Alice', 'Jose' ], 'Mary' has been forgotten
