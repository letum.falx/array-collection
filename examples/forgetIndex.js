const [collection] = require('./_data');

console.log(collection.count()); // returns 5
console.log(collection.indices().getOriginal()); // [ 0, 1, 2, 3, 4 ];

collection.forgetIndex(1);

console.log(collection.count()); // returns 4
console.log(collection.indices().getOriginal()); // [ 0, 1, 2, 3 ]
