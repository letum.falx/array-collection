const [collection] = require('./_data');

const newCollection = collection.slice(1, 3);
console.log(collection.count()); // returns 5
console.log(newCollection.count()); // returns 2
console.log(collection.map(entry => entry.name).getOriginal()); // displays [ 'John', 'Mary', 'Michael', 'Alice', 'Jose' ]
