const [collection] = require('./_data');

// passing a key
console.log(collection.every('name')); // returns true
console.log(collection.every('registered')); // returns false

// passing a test function
console.log(collection.every(value => value.birth.day > 0)); // returns true
console.log(collection.every(value => value.birth.year < 2000)); // returns false

// passing a key and value
console.log(collection.every('bloodType', 'O')); // returns true
console.log(collection.every('birth.month', 'January')); // returns false

// passing a key and test function
console.log(collection.every('birth.month', month => month !== 'February')); // returns true
console.log(collection.every('birth.day', day => day < 20)); // returns false

// passing a key, operator and a value
console.log(collection.every('birth.day', '<', 30)); // returns true
console.log(collection.every('birth.year', '<', 2000)); // returns false
