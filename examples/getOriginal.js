const [collection] = require('./_data');

console.log(collection.getOriginal());

// returns:
/*
[
  {
    name: 'John',
    birth: { month: 'January', day: 27, year: 2000 },
    registered: true,
    bloodType: 'O'
  },
  {
    name: 'Mary',
    birth: { month: 'January', day: 15, year: 1995 },
    registered: false,
    bloodType: 'O'
  },
  {
    name: 'Michael',
    birth: { month: 'December', day: 18, year: 1998 },
    registered: true,
    bloodType: 'O'
  },
  {
    name: 'Alice',
    birth: { month: 'May', day: 24, year: 1993 },
    registered: false,
    bloodType: 'O'
  },
  {
    name: 'Jose',
    birth: { month: 'September', day: 11, year: 2002 },
    registered: true,
    bloodType: 'O'
  }
]
*/
