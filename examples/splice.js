const [collection] = require('./_data');

const spliced = collection.splice(1, 3);

console.log(collection.count()); // returns 2

console.log(spliced.count()); // returns 3
