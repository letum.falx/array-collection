const [collection, seed] = require('./_data');

console.log(collection.isEmpty()); // returns false

collection.empty(); // empty collection

console.log(collection.isEmpty()); // returns true
