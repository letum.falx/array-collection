const [collection] = require('./_data');

// very last entry
console.log(collection.last().name); // displays 'Jose'

// using searchFunc
console.log(collection.last(entry => entry.birth.year < 1998).name); // displays 'Alice'

// using search function and default value
console.log(collection.last(entry => entry.birth.year > 2020, { name: 'Not Found' }).name); // displays 'Not Found'

// using default value only
collection.empty(); // empties the collection first
console.log(collection.last({ name: 'Not Found' }).name); // displays 'Not Found'
