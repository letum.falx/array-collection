const [collection] = require('./_data');

console.log(collection.count()); // returns 5

console.log(collection.pop().name); // displays 'Jose'

console.log(collection.count()); // returns 4
