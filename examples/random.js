const [collection, seed] = require('./_data');

console.log(collection.random()); // displays random item from the collection

collection.empty(); // empty collection

console.log(collection.random(null)); // displays null
