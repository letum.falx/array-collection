const [collection] = require('./_data');

// very first entry
console.log(collection.first().name); // displays 'John'

// using searchFunc
console.log(collection.first(entry => entry.birth.year < 1998).name); // displays 'Mary'

// using search function and default value
console.log(collection.first(entry => entry.birth.year > 2020, { name: 'Not Found' }).name); // displays 'Not Found'

// using default value only
collection.empty(); // empties the collection first
console.log(collection.first({ name: 'Not Found' }).name); // displays 'Not Found'
