const [collection, seed] = require('./_data');

console.log(collection.isNotEmpty()); // returns true

collection.empty(); // empty collection

console.log(collection.isNotEmpty()); // returns false
