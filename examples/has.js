const [collection, seed] = require('./_data');

console.log(collection.has(0)); // returns true
console.log(collection.has(seed.length)); // returns false
