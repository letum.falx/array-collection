const [collection, seed] = require('./_data');

console.log(collection.get(0) === seed[0]); // returns true
console.log(collection.get(seed.length, null) === null); // returns true
