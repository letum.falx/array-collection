const [collection] = require('./_data');

const entry = collection.shift();

console.log(entry.name); // displays 'John'

console.log(collection.search(entry)); // returns -1

collection.empty();

console.log(collection.shift(null)); // returns null
