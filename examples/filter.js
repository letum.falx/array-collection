const [collection, seed] = require('./_data');

seed.push(null);

console.log(collection.count()); // returns 6

// passing no arguments
console.log(collection.filter().count()); // returns 5, null is removed

// passing a test function
console.log(collection.filter(value => value && value.name === 'Michael').count()); // returns 1

// passing a property key
console.log(collection.filter('registered').count()); // returns 3

// passing a key and value
console.log(collection.filter('birth.month', 'January').count()); // returns 2

// passing a key and test function
console.log(collection.filter('birth.month', value => value === 'January').count()); // returns 2

// passing a key, operator, and a vallue
console.log(collection.filter('birth.month', '===', 'January').count()); // returns 2
