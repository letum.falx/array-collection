const [collection, seed] = require('./_data');

// passing a value
console.log(collection.contains(seed[0])); // true
console.log(collection.contains(seed[seed.length])); // false

// passing a value search function
console.log(collection.contains(value => value.name === 'Michael')); // true
console.log(collection.contains(value => value.birth.month === 'February')); // false

// passing key and value
console.log(collection.contains('birth.year', 2000)); // true
console.log(collection.contains('birth.month', 'November')); // false

// passing key and value search function
console.log(collection.contains('birth.year', year => year > 2000)); // true
console.log(collection.contains('birth.day', day => day < 10 || day > 28)); // false

// passing key, operator and value
console.log(collection.contains('birth.year', '==', '2000')); // true because of loose comparison
console.log(collection.contains('birth.year', '===', '2000')); // false because of strict comparison
