const [collection] = require('./_data');

// original collection
console.log(collection.map(entry => entry.birth.year).getOriginal()); // [ 2000, 1995, 1998, 1993, 2002 ]

// use nothing
console.log(collection.sortDesc().map(entry => entry.birth.year).getOriginal()); // [ 2000, 1995, 1998, 1993, 2002 ]

// using property key
console.log(collection.sortDesc('birth.year').map(entry => entry.birth.year).getOriginal()); // [ 2002, 2000, 1998, 1995, 1993 ]

// using sort function
console.log(collection.sortDesc((a, b) => a.birth.year - b.birth.year).map(entry => entry.birth.year).getOriginal()); // [ 2002, 2000, 1998, 1995, 1993 ]

// using property key and sort function
console.log(collection.sortDesc('birth.year', (yearA, yearB) => yearA - yearB).map(entry => entry.birth.year).getOriginal()); // [ 2002, 2000, 1998, 1995, 1993 ]
