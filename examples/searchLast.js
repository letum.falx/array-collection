const [collection, seed] = require('./_data');

// passing a value
console.log(collection.searchLast(seed[0])); // returns 0
console.log(collection.searchLast({})); // returns -1

// passing a test function
console.log(collection.searchLast(value => value.birth.month === 'January')); // returns 1
console.log(collection.searchLast(value => value.birth.year > 2020)); // returns -1

// passing a key and value
console.log(collection.searchLast('birth.month', 'January')); // returns 1
console.log(collection.searchLast('birth.year', '1995')); // returns -1, we are using strict comparison under the hood

//passing a key and a test function
console.log(collection.searchLast('birth.year', year => year < 2000)); // returns 3
console.log(collection.searchLast('birth.day', day => day < 10)); // returns -1

// passing a key, operator and a value
console.log(collection.searchLast('birth.month', '!==', 'January')); // returns 4
console.log(collection.searchLast('birth.month', '===', 'February')); // returns -1
