const [collection, seed] = require('./_data');

// passing a entry value
console.log(collection.clone().forgetLast(seed[0]).map(entry => entry.name).getOriginal()); // returns [ 'Mary', 'Michael', 'Alice', 'Jose' ], 'John' has been forgotten

// passing a search function
console.log(collection.clone().forgetLast(entry => entry.birth.year < 2000).map(entry => entry.name).getOriginal()); // [ 'John', 'Mary', 'Michael', 'Jose' ], 'Alice' has been forgotten

// passing a property key and value
console.log(collection.clone().forgetLast('birth.month', 'January').map(entry => entry.name).getOriginal()); // returns [ 'John', 'Michael', 'Alice', 'Jose' ], 'Mary' has been forgotten

// passing a property key and a search function
console.log(collection.clone().forgetLast('birth.year', year => year < 2000).map(entry => entry.name).getOriginal()); // [ 'John', 'Mary', 'Michael', 'Jose' ], 'Alice' has been forgotten

// passing a property key, operator and value
console.log(collection.clone().forgetLast('birth.year', '<', 2000).map(entry => entry.name).getOriginal()); // [ 'John', 'Mary', 'Michael', 'Jose' ], 'Alice' has been forgotten
