const [collection, seed] = require('./_data');

// passing a value
console.log(collection.search(seed[0])); // returns 0
console.log(collection.search({})); // returns -1

// passing a test function
console.log(collection.search(value => value.birth.month === 'January')); // returns 0
console.log(collection.search(value => value.birth.year > 2020)); // returns -1

// passing a key and value
console.log(collection.search('birth.year', 1995)); // returns 1
console.log(collection.search('birth.year', '1995')); // returns -1, we are using strict comparison under the hood

//passing a key and a test function
console.log(collection.search('birth.year', year => year < 2000)); // returns 1
console.log(collection.search('birth.day', day => day < 10)); // returns -1

// passing a key, operator and a value
console.log(collection.search('birth.month', '!==', 'January')); // returns 2
console.log(collection.search('birth.month', '===', 'February')); // returns -1
