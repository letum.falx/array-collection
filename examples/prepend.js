const [collection] = require('./_data');

// passing a value

const valueCollection = collection.clone().prepend({
  name: 'Junior',
  birth: {
    day: 12,
    month: 'June',
    year: 2010
  }
});

console.log(valueCollection.get(0)); // returns { name: 'Junior', birth: { day: 12, month: 'June', year: 2010 } }
console.log(valueCollection.count()); // returns 6

// passing an array of values
const arrayCollection = collection.clone().prepend([
  {
    name: 'Junior',
    birth: {
      day: 12,
      month: 'June',
      year: 2010
    }
  },
  {
    name: 'Senior',
    birth: {
      day: 15,
      month: 'October',
      year: 1980
    }
  },
]);

console.log(arrayCollection.get(0)); // returns { name: 'Junior', birth: { day: 12, month: 'June', year: 2010 } }
console.log(arrayCollection.get(1)); // returns { name: 'Senior', birth: { day: 15, month: 'October', year: 1980 } }
console.log(arrayCollection.count()); // returns 7
