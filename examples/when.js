const [collection] = require('./_data');

const executor = condition => collection
  .when(
    condition,
    collection => {
      const entry = collection.get(0);
      entry.name = `${entry.name}-true`;
    },
    collection => {
      const entry = collection.get(1);
      entry.name = `${entry.name}-false`;
    }
  );

console.log(executor(true).get(0).name); // displays: John-true
console.log(executor(false).get(1).name); // displays: Mary-false
