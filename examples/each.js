const [collection, seed] = require('./_data');

collection.each((value, index) => {
  value.name = index; // replace the name with the index
});

console.log(collection.get(0).name); // returns 0
console.log(collection.get(2).name); //returns 2
