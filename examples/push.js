const [collection] = require('./_data');

// passing a value

const valueCollection = collection.clone().push({
  name: 'Junior',
  birth: {
    day: 12,
    month: 'June',
    year: 2010
  }
});

console.log(valueCollection.get(valueCollection.lastIndex())); // returns { name: 'Junior', birth: { day: 12, month: 'June', year: 2010 } }
console.log(valueCollection.count()); // returns 6

// passing an array of values
const arrayCollection = collection.clone().push([
  {
    name: 'Junior',
    birth: {
      day: 12,
      month: 'June',
      year: 2010
    }
  },
  {
    name: 'Senior',
    birth: {
      day: 15,
      month: 'October',
      year: 1980
    }
  },
]);

const lastIndex = arrayCollection.lastIndex();

console.log(arrayCollection.get(lastIndex - 1)); // returns { name: 'Junior', birth: { day: 12, month: 'June', year: 2010 } }
console.log(arrayCollection.get(lastIndex)); // returns { name: 'Senior', birth: { day: 15, month: 'October', year: 1980 } }
console.log(arrayCollection.count()); // returns 7
