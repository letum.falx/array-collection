const [collection] = require('./_data');

// getting the average birth year
const total = collection.reduce((acc, curr) => acc + curr.birth.year, 0); // get the sum of all birth year

console.log(Math.round(total / collection.count())); // displays 1997
