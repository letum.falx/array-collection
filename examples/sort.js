const [collection] = require('./_data');

// original collection
console.log(collection.map(entry => entry.birth.year).getOriginal()); // [ 2000, 1995, 1998, 1993, 2002 ]

// use nothing
console.log(collection.sort().map(entry => entry.birth.year).getOriginal()); // [ 2000, 1995, 1998, 1993, 2002 ]

// using property key
console.log(collection.sort('birth.year').map(entry => entry.birth.year).getOriginal()); // [ 1993, 1995, 1998, 2000, 2002 ]

// using sort function
console.log(collection.sort((a, b) => a.birth.year - b.birth.year).map(entry => entry.birth.year).getOriginal()); // [ 1993, 1995, 1998, 2000, 2002 ]

// using property key and sort function
console.log(collection.sort('birth.year', (yearA, yearB) => yearA - yearB).map(entry => entry.birth.year).getOriginal()); // [ 1993, 1995, 1998, 2000, 2002 ]
