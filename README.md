# Array Collection

A utility wrapper for the array. Inspired by [Laravel](https://laravel.com/)'s Collection and its JS port [collect.js](https://www.npmjs.com/package/collect.js).

This package has its own implementation of each method.

## Installation

For NPM:

```bash
npm install @letumfalx/array-collection
```

For Yarn:

```bash
yarn add @letumfalx/array-collection
```

## Notes

- This will just wrap the array to the collection instance maintaining its reference, meaning mutating the array outside the collection instance will also mutate the internal array reference.
- The methods with overloads uses the number and type of arguments passed to determine what kind of behavior it will do. For example, `contains` has 4 overloads:
  - passing `1 argument` that is not a function will check if that value is in the collection
  - passing `1 argument` that is a function will use that function to check if there are entries that passes that function
  - passing `2 arguments` will use the first argument as the key of the object to search and the second argument as the comparing value
  - passing `3 arguments` will use the first argument as the key of the object to search, the second argument as the operator and the third as the value to compare
- The `defaultValue`s are always defaults to `NaN` if not given.
- This will just copy the reference of the entry from the internal array if it is deriving a new collection.

## API

Here are the list of the methods for the collection. Check the [Definition File](./src/Collection.d.ts) for more information.

- `constructor (data: T[] = [])`
- `clone (): Collection<T>`
- `contains (value: T): Boolean`
- `contains (search: (value: T, index: Number) => Boolean): Boolean`
- `contains<V> (key: String, value: V): Boolean`
- `contains<V> (key: String, operator: Operator, value: V): Boolean`
- `count (): Number`
- `each (iterator: (value: T, index: Number) => void): this`
- `empty (): this`
- `every (key: String): Boolean`
- `every (testFunc: (value: T, index: Number) => Boolean): Boolean`
- `every<V> (key: String, value: V): Boolean`
- `every<V> (key: String, testFunction: (value: V, index: Number) => Boolean): Boolean`
- `every<V> (key: String, operator: Operator, value: V): Boolean`
- `filter (): Collection<T>`
- `filter (testFunction: (value: T, index: Number) => Boolean): Collection<T>`
- `filter (key: String): Collection<T>`
- `filter<V> (key: String, value: V): Collection<T>`
- `filter<V> (key: String, testFunction: (value: V, index: Number) => Boolean): Collection<T>`
- `filter<V> (key: String, operator: Operator, value: V): Collection<T>`
- `first (): T`
- `first (searchFunc: (entry: T) => Boolean): T`
- `first<V> (defaultValue: V): T`
- `first<V> (searchFunc: (entry: T) => Boolean, defaultValue: V): T`
- `forget (value: T): this`
- `forget<V> (key: String, value: V): this`
- `forget<V> (key: String, testFunction: (value: V, index: Number) => Boolean): this`
- `forget<V> (key: String, operator: Operator, value: V): this`
- `forgetIndex (index: Number): this`
- `forgetLast (value: T): this`
- `forgetLast<V> (key: String, value: V): this`
- `forgetLast<V> (key: String, testFunction: (value: V, index: Number) => Boolean): this`
- `forgetLast<V> (key: String, operator: Operator, value: V): this`
- `get<V> (index: Number, defaultValue: V): T`
- `getOriginal (): T[]`
- `has (index: Number): Boolean`
- `indices(): Collection<Number>`
- `isEmpty (): Boolean`
- `isNotEmpty (): Boolean`

- `last (): T`
- `last (searchFunc: (entry: T) => Boolean): T`
- `last<V> (defaultValue: V): T`
- `last<V> (searchFunc: (entry: T) => Boolean, defaultValue: V): T`
- `lastIndex (): Number`
- `map<V> (transformer: (value: T, index: Number) => V): Collection<V>`
- `pop<V> (defaultValue: V): T`
- `prepend (value: T): this`
- `prepend (values: T[]): this`
- `pull<V> (index: Number, defaultValue: V): T`
- `push (value: T): this`
- `push (values: T[]): this`
- `random<V> (defaultValue: V): T`
- `randomPull<V> (defaultValue: V): T`
- `reduce<I, A, R> (reducer: (accumulator: A, current: T, index: Number) => R, initialValue: I): R`
- `reject (): Collection<T>`
- `reject (testFunction: (value: T, index: Number) => Boolean): Collection<T>`
- `reject (key: String): Collection<T>`
- `reject<V> (key: String, value: V): Collection<T>`
- `reject<V> (key: String, testFunction: (value: V, index: Number) => Boolean): Collection<T>`
- `reject<V> (key: String, operator: Operator, value: V): Collection<T>`
- `reverse (): Collection<T>`
- `search (value: T): Number`
- `search<V> (key: String, value: V): Number`
- `search<V> (key: String, testFunction: (value: V, index: Number) => Boolean): Number`
- `search<V> (key: String, operator: Operator, value: V): Number`
- `searchLast (value: T): Number`
- `searchLast<V> (key: String, value: V): Number`
- `searchLast<V> (key: String, testFunction: (value: V, index: Number) => Boolean): Number`
- `searchLast<V> (key: String, operator: Operator, value: V): Number`
- `shift<V> (defaultValue: V): T`
- `slice (startIndex: Number, endIndex: Number): Collection<T>`
- `sort (): Collection<T>`
- `sort (sortFunc: (a: T, b: T) => Number): Collection<T>`
- `sort (key: String): Collection<T>`
- `sort<V> (key: String, sortFunc: (a: V, b: V) => Number): Collection<T>`
- `sortDesc (): Collection<T>`
- `sortDesc (sortFunc: (a: T, b: T) => Number): Collection<T>`
- `sortDesc (key: String): Collection<T>`
- `sortDesc<V> (key: String, sortFunc: (a: V, b: V) => Number): Collection<T>`
- `splice (startIndex: Number, endIndex: Number, value: T): Collection<T>`
- `splice (startIndex: Number, endIndex: Number, value: T[]): Collection<T>`
- `splice (startIndex: Number, endIndex: Number, value: Collection<T>): Collection<T>`
- `toJson (): String`
- `when (condition: Boolean, ifHandler: (collection: this) => void, elseHandler: (collection: this) => void): this`

## License

MIT
